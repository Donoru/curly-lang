# This is the interpreter, I will soon add a way to compile into Batch
# I haven't used Python in a while so you can expect some errors :')

fn = "example.crl"
file = open(fn, "r")
vars = {}
def error(text):
  print(text)
  print("\t" + original)
  print("Have a nice day")
for line in file:
  line = line.strip()
  original = line
  if line.startswith("writeln { \"") and line.endswith("\" }"):
    line = line.replace("writeln { \"", "", 1)[0:-3]
    print(line)
  elif line.startswith("write { \"") and line.endswith("\" }"):
    line = line.replace("write { \"", "", 1)[0:-3]
    print(line, end = "")
  elif line.startswith("read { \"") and line.endswith("\" }"):
    line = line.replace("read { \"", "", 1)[0:-3]
    input(line)
  elif line.startswith("var ") and line.split(" ")[2] == "=":
    line = line.replace("var ", "", 1)
    vars[line.split(" ")[0]] = line.replace(line.split(" ")[0] + " = ", "", 1)
    var = line.replace(line.split(" ")[0] + " = ", "", 1)
    if var.startswith("\"") and var.endswith("\""):
      vars[line.split(" ")[0]] = var.replace("\"", "", 1)[0:-1]
    else:
      try:
        var = int(var)
      except:
        try:
          var = float(var)
        except:
          try:
            var = vars[var]
          except:
            error("Unknown variable: \"" + var + "\"")
          else:
            vars[line.split(" ")[0]] = var
        else:
          vars[line.split(" ")[0]] = var
      else:
        vars[line.split(" ")[0]] = var
  elif line == "":
    pass
  elif line.startswith("btw =>"):
    pass
  else:
    error("Invalid syntax")